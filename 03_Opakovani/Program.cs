﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03_Opakovani
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            1. Promenne jsou zaklad. Postupujte podle nasledujicich jednoradkovych komentaru.  
            */

            // Na nasledujicim radku deklarujte novou promennou typu Random.

            // Na nasledujicim radku do nove promenne priradte novy generator nahodnych cisel: new Random();

            // Na nasledujicim radku definujte novou promenou typu int.

            // Na nasledujicim radku do nove promenne priradte nahodne cislo mezi 0-10.

            // Na nasledujicim radku vypiste hodnotu v teto (int) promenne.



            /*
            2. Jak cist data ze vstupu.
            V consolove aplikaci pouzivame prikaz: Console.ReadLine();
            Tento prikaz zastavi chod programu, dokud uzivatel nezada do console nejaky text a nestiskne "Enter".
            Po stisku "Enter" tento prikaz vrati zadany text. "Vraceny" text lze ulozit do promenne. 
            Zkuste si odkomentovat a spustit nasledujici tri radky kodu.
            */

            //Console.WriteLine("Jak se jmenujes?");
            //string uzivatelskyVstup = Console.ReadLine();
            //Console.WriteLine("Uzivatel se jmenuje: " + uzivatelskyVstup);


            /*
            Pokud "vraceny" text do zadne promenne neulozime, program ho proste zahodi a nejde ho jiz nijak dohledat.
            Tuto castecnou funkcionalitu (cekani na Enter) jsme pouzivali doposud.
            
            !POZOR!: Console.Readline(); vraci jedine a pouze STRING
            */


            /*
            3. Spojovani stringu.
            Vime, ze operator "+" dokaze secist dve cisla. Kdyz ho ale pouzijeme pro text, bude spojovat.
            Situace, kdy kombinujeme cislo s textem dohromady muzete videt v prikladu 2:
            https://docs.google.com/document/d/1HH1LXIbNAHNbPpo24M848vMFBowrmbtKoay07uA8bPs/edit?usp=sharing

            Zkuste si spustit nasledujici program. Seznamte se. Pokud je vse jasne, rozsirte dotaznik o
             - Barvu oci
             - velikost bot
            */

            /*
            string profilUzivatele;

            Console.WriteLine("Jste muz nebo zena?");
            profilUzivatele = "Uzivatelem je " + Console.ReadLine();

            Console.WriteLine("Jake je vase krestni jmeno?");
            profilUzivatele = profilUzivatele + ", jmenem " + Console.ReadLine();

            Console.WriteLine("Mate radeji psi nebo kocky?");
            profilUzivatele = profilUzivatele + ". Ma radeji " + Console.ReadLine();

            Console.WriteLine(profilUzivatele);
            */


            /*
            4. Prevest string na cislo. Nasledujici program prevadi string na cislo (int) pomoci prikazu:
            Int32.Parse(text);
            Tento prikaz vysledne cislo opet "vraci", tedy je mozne ulozit do promenne.
            Upravte nasledujici program tak, aby uzivatele vyzval k zadani cisla, a pote uzivatelsky 
            vstup precetl a prevedl na cislo. Cislo vypiste.
            */

            //int cislo = Int32.Parse("99");
            //Console.WriteLine(cislo);


            /*
            BONUS. Bezpecne prevadeni.
            Zkuste k reseni prikladu 4 nastudovat a vyuzit prikaz: Int32.TryParse();
            V pripade, ze uzivatel nezada cele cislo, vypiste: "Neplatny vstup! Priset zadejte prosim cislo."
            */

            /*
            BONUS. Interpolovany string.
            Zkuste k vypisu prikladu 4 nastudovat pouziti textu ve tvaru: $"{}"
            anglicky: Interpolated string
            cesky: https://docs.microsoft.com/cs-cz/dotnet/csharp/tutorials/string-interpolation
            */

            Console.ReadLine();
        }
    }
}
